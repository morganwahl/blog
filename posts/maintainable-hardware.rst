.. title: Sustainable Hardware
.. date: 2022-09-03
.. type: text
.. category: public policy
.. tags: policy, software, hardware, sustainability

Here's a proposed policy to address several problems:

All computers sold with pre-loaded software must meet at least one of the two following conditions:

- Open: The source-code of the software and the development tools required to build and install the software is publicly available, in perpetuity, and is licensed under an open-source license.
- Replaceable: Documentation is made publicly available, in perpetuity, that is sufficient to write and install software on the computer that accomplishes all the same tasks as the pre-loaded software, and it is possible to replace the pre-loaded software without special tools.

If the pre-loaded software on the computer connects to any services on other computers, the communications protocol must be documented and the computer's owner must be able to choose what computers it connects to.

This would apply to the software in things like laptops, but also embedded software in cars, appliances, phones, and the myriad other things with computers in them. The manufacturer can
choose to either "show their work" (the open approach) so that the public can see exactly what the devices will do, or keep the software secret, but facilitate its replacement.

For connections to other services, only the "replacable" option really makes sense.

This would allow hardware to continue to function even when abandoned by its manufacturer, while still giving manufacturers the option of keeping their software secret if they choose.
Either one would be minimal effort, since it just requires the publishing of what the manufacturer would already have a copy of for their own use. Public organizations such as libraries could even handle the archiving and making available "in perpetuity" of the software or documentation.

For some systems, it would greatly complicate the design to make the software easily replacable after manufacturing, or replacing the software would require regulartory re-approval. These would have 
to go with the "Open" option. This makes sense, because these systems are effectively static tools, and exactly what they will do should be thoroughly knowable by the people using them.

Sometimes manufacturers want to keep their software secret. This tends to be much more complicated software, and thus it's less onerous to provide a mechanism to replace it. Thus, things like phones and laptops would likely go the "Replaceable" route. This is more or less status-quo, the main improvement of this policy is that hardware documentation would be available
to anyone writing software for that hardware.

These requirements apply to each computer individually, of which there may be many inside a single manufactured "product". In a laptop for example, the controller in an SSD may be an embeded computer.
The manufacturer could either make it easily replacable (perhaps with a usb port and documented debugging interface?), or simply release its source code. The OS and bootloader on the other
hand, may remain secret, but the hardware interfaces they use would then need to be documented.

Another example might be a car. Some of the computers would simply have their source code released, particularly critical ones with safety concerns. This allows for more public scrutiny of
such systems, which is a good thing. Other computers in the car, such as the one controlling the stereo or telephone, might stay secret, but be replaceable.

The "connections to other services" should apply to connections between computers _within_ a product as well. This allows owners to replace parts to repair the product.

The definition of "computer" is turing-completeness and programmability. Even if it is only programmed once, that still counts. If it can _only_ be programmed once, then only the "Open" option is available.
