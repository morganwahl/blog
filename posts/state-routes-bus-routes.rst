.. title: State Routes as Bus Routes
.. date: 2022-09-03
.. type: text
.. category: public policy
.. tags: transportation

Policy proposal: Every state route/state highway (including federally funded ones) is also a bus route.

Since the roads are publicly paid for, and designed for use by motor vehicles, there should be public motor vehicles available to use them. This is most efficiently done using buses.

Making state routes reliably also bus routes allows residents to easily navigate whether driving themselves or taking a bus. It also means development that is accessible by road is more likely
to be accessible by people who can't or don't drive.

A state route network is probably not a well-designed bus network. Some route redesigns would make sense. The state might choose to no longer fund the maintenance of some highways if they
don't make sense as bus routes. The towns such routes run through might not like this, but ultimately if it doesn't make sense to run a bus there, then it doesn't make sense for the whole
state to pay for the road itself.

The state buses would ideally be well-integrated with other public transportation. That other public transportation may adjust its services to take the state ones into account. They might
be able to provide more and better service since some of their load is now taken on by state buses.

Interstates in particular lend themselves to local (stops at every exit) and express (stops at cities?) service.

This policy would allow for some efficiencies by doubling-up on signage and route design.
