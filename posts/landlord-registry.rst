.. title: Landlord Registry
.. date: 2022-09-03
.. type: text
.. category: public policy
.. tags: policy, housing 

Policy proposal: Implement a registry of landlords to track rents, lease terms, and landlord income.

Land ownership is considered a matter of public record, and details about property size, building, value, tax, mortgages, and ownership history are carefully maintained by the town. Information on
the rent paid and lease terms is not. Nor is there any record of who _is_ a landlord and there responsible to provide good housing to their tenants.

An registry of landlords would list all properties rented, amount paid in rent, and the lease terms. The identity of the individual renters would not be included if they lived there, but the
renters of non-residental properties or corportate renters would be indentified.

Landlords would be required to keep this information up-to-date.

Such a registry would provide a very valuable source of data on the current state of rental housing.

The registry could also provide digital services to tenants and landlords, such as easy rent payments or lease signatures and recording.
