.. title: Residential Deductions
.. date: 2022-09-03
.. type: text
.. category: public policy
.. tags: housing, taxes

Policy proposal: Implement residential property-tax deductions as simple payments to all residents.

In Massachusetts, towns are largely funded by property taxes. The town picks the tax rate (per assessed value of the property) and an amount to deduct from the tax for each property if the owner lives at that property.

For people who rent, the property tax is effectively included in the rent (otherwise the landlord would be losing money). However, they don't get the resident deduction. (Yes, if the landlord lives in the building, the build does get it. But the amount per housing unit, and per person is much lower). This makes the tax somewhat regressive.

Instead of subsidizing housing by paying property owners, simply make a payment to each resident of the town. (With a 1099 for income tax.) This means renters get the same deduction as owners. It also makes the property tax slightly progressive.
