.. title: Robots
.. date: 2021-03-04
.. type: text

The trope of "robots" in stories is a way to include slavery in a story without having to
deal with any of the real-life terrible things about enslaving humans.

In fact the real-life obsession among some people of making machines as human-like as they
can may stem from a desire to have control over real humans. There aren't many engineering
problems that call for a machine to take the form of a human, certainly not enough to
justify the interest in such-shaped machines.

The Jetsons is a white American fantasy of subservient humans with none of the "downsides"
(from a white perspective) of actual black people.

The "robot uprising" trope is perhaps an echo of fear of slave revolts in the U.S.

"Robots" are almost always depicted as just impractically (and impossibly) human. The main
thing that separates the idea of a "robot" from a "machine" is some attempt at simulating
a human, even if it's just a face.

Experiment: put a face on your dishwasher. Does it seem like a robot now?

There are definitions of "robot" that refer to some idea of a machine that senses its
environment and responds, and is somehow more "independent" of humans. But this is not
a very precise distinction, and is often just one of degree. A furnace with a thermostat
senses its environment and responds, but most people don't think of it as a robot.

The robot trope may be a re-casting of the older trope of animals that act like just
differently-shaped humans. It's _possible_ there's some of the same motivation there: if you're
used to working with domesticated animals, you might think an animal that was more human
would be like a human you could boss around. But I think the reverse is more likely: putting
human attributes on animals is a way to describe their _uncontrollableness_. A smaller example
of the pattern of putting human thoughts in various things to explain their unpredictability
or complexity (e.g. gods controlling weather, yokai, etc.), and make it seem less scary.

